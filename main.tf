data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

resource "aws_eip" "web-eip" {
  vpc = true
}

resource "aws_security_group" "sg-tf-assignment" {
  name        = "tf-assignment-sg"
  description = "Security Group for Terraform Built Instance"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.web-server.id
  allocation_id = aws_eip.web-eip.id
  depends_on = [
    aws_instance.web-server
  ]
}

resource "aws_instance" "web-server" {
  ami           = data.aws_ami.amazon-linux-2.id
  instance_type = "t3.micro"
  key_name      = var.ec2_key_name

  vpc_security_group_ids = [
    aws_security_group.sg-tf-assignment.id
  ]
  provisioner "remote-exec" {
    inline = [
      "sudo yum -y update",
      "sudo amazon-linux-extras enable nginx1",
      "sudo yum -y install nginx",
      "sudo systemctl start nginx",
      "sudo systemctl enable nginx"
    ]
  }
  connection {
    user        = "ec2-user"
    private_key = file("${var.private_key_path}")
    host        = self.public_ip
  }
}
