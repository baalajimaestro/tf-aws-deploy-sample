# terraform-example

Terraform Module for AWS EC2 Instance creation
## Providers

| Name | Version |
|------|---------|
| aws  | ~> 4.0  |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| vpc\_id | VPC to create the instance in | `string` | n/a | yes |
| aws\_region | AWS Region for develop infra | `string` | `"us-east-1"` | no |
| aws\_profile | AWS Profile to use | `string` | n/a | yes |
| ec2\_key\_name | Keypair name in AWS | `string` | n/a | yes |
| private\_key\_path | Path to private key for the corresponding keypair | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| instance\_ip\_addr | Elastic IP of the created instance |
| instance\_id | EC2 instance ID |
