output "instance_ip_addr" {
  value = aws_eip.web-eip.public_ip
}

output "instance_id" {
  value = aws_instance.web-server.id
}