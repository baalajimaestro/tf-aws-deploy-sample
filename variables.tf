variable "vpc_id" {
  type        = string
  description = "The VPC to spin up the EC2 in"
}

variable "aws_profile" {
  type        = string
  description = "The AWS Profile"
}

variable "aws_region" {
  type        = string
  description = "The AWS Region"
  default     = "us-east-1"
}

variable "ec2_key_name" {
  type        = string
  description = "EC2 Key Name"
}

variable "private_key_path" {
  type        = string
  description = "Private Key for connecting to EC2 instance"
}